package io.wisoft.seminar.department;

import java.util.List;

public interface DepartmentService {

    List<Department> getDepartmentAll();

    List<Department> getDepartmentListByNo(final List<String> studentNo);

}
