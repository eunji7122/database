package io.wisoft.seminar.department;

import io.wisoft.seminar.configure.ConnectionMaker;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class SimpleDepartmentService implements DepartmentService {

    final ConnectionMaker connectionMaker =  new ConnectionMaker();

    @Override
    public List<Department> getDepartmentAll() {
        List<Department> departmentList = new ArrayList<>();

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
            final DepartmentService service = sqlSession.getMapper(DepartmentService.class);
            departmentList = service.getDepartmentAll();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return departmentList;
    }

    @Override
    public List<Department> getDepartmentListByNo(List<String> studentNoList) {
        List<Department> departmentList = new ArrayList<>();

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
            final DepartmentService service = sqlSession.getMapper(DepartmentService.class);
            departmentList = service.getDepartmentListByNo(studentNoList);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return departmentList;
    }
}
