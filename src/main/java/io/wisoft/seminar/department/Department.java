package io.wisoft.seminar.department;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Department {

    private String code;
    private String name;
    private String location;

    public Department() {
    }

    public Department(String code, String name, String location) {
        this.code = code;
        this.name = name;
        this.location = location;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("dept_code", code)
                .append("dept_name", name)
                .append("dept_loc", location)
                .toString();
    }
}
