package io.wisoft.seminar.drama;

import io.wisoft.seminar.configure.ConnectionMaker;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class SimpleDramaService implements DramaService {

    final ConnectionMaker connectionMaker =  new ConnectionMaker();

    @Override
    public List<Drama> getDramaAll() {
        List<Drama> dramaList = new ArrayList<>();

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
            final DramaService service = sqlSession.getMapper(DramaService.class);
            dramaList = service.getDramaAll();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return dramaList;
    }

    @Override
    public List<Drama> getBroadListByBrd(List<String> dramaBrd) {
        List<Drama> DramaList = new ArrayList<>();

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()){
            final DramaService service = sqlSession.getMapper(DramaService.class);
            DramaList = service.getBroadListByBrd(dramaBrd);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return  DramaList;
    }

    @Override
    public List<Drama> getProviderAll() {
        List<Drama> dramaList = new ArrayList<>();

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()){
            final DramaService service = sqlSession.getMapper(DramaService.class);
            dramaList = service.getProviderAll();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return dramaList;
    }

    @Override
    public List<Drama> getDateIsNull() {
        List<Drama> dramaList = new ArrayList<>();

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()){
            final DramaService service = sqlSession.getMapper(DramaService.class);
            dramaList = service.getDateIsNull();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return dramaList;
    }

    @Override
    public int updateDramaDate() {
        int result = 0;

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
            final DramaService service = sqlSession.getMapper(DramaService.class);
            result = service.updateDramaDate();

            sqlSession.commit();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
