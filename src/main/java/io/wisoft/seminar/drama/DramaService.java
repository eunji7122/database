package io.wisoft.seminar.drama;

import java.util.List;

public interface DramaService {

    List<Drama> getDramaAll();

    List<Drama> getBroadListByBrd(final List<String> dramaBrd);

    List<Drama> getProviderAll();

    List<Drama> getDateIsNull();

    int updateDramaDate();

}
