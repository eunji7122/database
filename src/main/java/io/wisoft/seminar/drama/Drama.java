package io.wisoft.seminar.drama;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Drama {

    private String code;
    private String name;
    private String broad;
    private String provider;
    private String date;

    public Drama() {
    }

    public Drama(String code, String name, String broad, String provider, String date) {
        this.code = code;
        this.name = name;
        this.broad = broad;
        this.provider = provider;
        this.date = date;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("drm_code", code)
                .append("drm_name", name)
                .append("drm_brd", broad)
                .append("drm_prd", provider)
                .append("drm_opdate", date)
                .toString();
    }
}
