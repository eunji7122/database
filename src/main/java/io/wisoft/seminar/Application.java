package io.wisoft.seminar;

import io.wisoft.seminar.department.SimpleDepartmentService;
import io.wisoft.seminar.department.Department;
import io.wisoft.seminar.department.DepartmentService;
import io.wisoft.seminar.drama.Drama;
import io.wisoft.seminar.drama.DramaService;
import io.wisoft.seminar.drama.SimpleDramaService;
import io.wisoft.seminar.employee.*;
import org.apache.commons.lang3.time.StopWatch;

import java.util.Arrays;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        final DepartmentService departmentService = new SimpleDepartmentService();
        final StopWatch stopWatch = new StopWatch();
        int count = 0;

        List<Department> studentList;
        System.out.println();

        System.out.println("#1 부서코드, 이름, 위치 조회");
        studentList = departmentService.getDepartmentAll();
        studentList.forEach(System.out::println);
        System.out.println();

        final EmployeeService employeeService = new SimpleEmployeeService();

        List<Employee> employeeList;
        System.out.println();

        System.out.println("전체 사용자 목록 조회");
        employeeList = employeeService.getEmployeeAll();
        employeeList.forEach(System.out::println);
        System.out.println();

        System.out.println("#3 드라마의 코드와 이름 조회");
        final DramaService dramaService = new SimpleDramaService();
        List<Drama> DramaList = dramaService.getDramaAll();
        DramaList.forEach(System.out::println);
        System.out.println();

        System.out.println("#4 드라마 방영사가 KBC이거나 SBC인 드라마 조회");
        List<String> DramaList2 = Arrays.asList("KBC", "SBC");
        DramaList = dramaService.getBroadListByBrd(DramaList2);
        DramaList.forEach(System.out::println);
        System.out.println();

        System.out.println("#5 드라마 제작사 조회 (중복된 값 제거)");
        List<Drama> dramaList3 = dramaService.getProviderAll();
        dramaList3.forEach(System.out::println);
        System.out.println();

        System.out.println("#6 연예관계자들의 급여의 총합과 평균 급여액 계산");
        List<EmployeeSalary> employeeListSalary = employeeService.getSalaryAll();
        employeeListSalary.forEach(System.out::println);
        System.out.println();

        System.out.println("#7 방영일자가 아직 확정되지 않은 드라마 이름 조희");
        List<Drama> dramaList7 = dramaService.getDateIsNull();
        dramaList7.forEach(System.out::println);
        System.out.println();

        System.out.println("#8 연예관계자에 대해 연예관계자의 이름과 직속 상사의 이름 조회");
        List<EmployeeMgt> employeeMgtList = employeeService.getNameMgt();
        employeeMgtList.forEach(System.out::println);
        System.out.println();

        System.out.println("#9 연예관계자에 대해 이름과 급여 조회 (급여는 내림차순 정렬, 동일 급여인 경우 " +
                "이름 오름차순 정렬");
        List<Employee> employeeList9 = employeeService.getNameSalary();
        employeeList9.forEach(System.out::println);
        System.out.println();

        System.out.println("#10 모든 연예관계자를 직급별로 그룹화하고, 평균 급여액이 5000 이상인 직급에 대해 " +
                "연예 관계자의 직급, 평균 급여액, 최소 급여액, 최대 급여액을 검색");
        List<EmployeeRank> employeeRanks = employeeService.getEmployeeRank();
        employeeRanks.forEach(System.out::println);
        System.out.println();

        System.out.println("#11 모든 연예관계자의 평균 급여액보다 많은 급여를 받는 연예관계자의 이름과 " +
                "급여를 검색");
        List<Employee> employeeList11 = employeeService.getMoreAvg();
        employeeList11.forEach(System.out::println);
        System.out.println();

        System.out.println("#12 방영일자가 확정되지 않은 드라마의 방영일자가 2013-05-01로 편성");
        int updateDate = dramaService.updateDramaDate();
        System.out.println(updateDate + "건이 변경되었습니다.");
        System.out.println();

        System.out.println("#13 연예관계자 김수현 씨가 대리에서 실장으로 승진하고 급여가 20% 증가 변경");
        int updateRankSalary = employeeService.updateRankSalary();
        System.out.println(updateRankSalary + "건이 변경되었습니다.");
        System.out.println();

        System.out.println("#14 우리 회사에 한 명의 임원이 등록되었습니다. 코드는 E903, 이름은 손진현, " +
                "관리자는 E901, 급여는 4000입니다. 알맞게 등록하시오.");
        final Employee employee = new Employee("E903", "손진현", "E901", 4000);
        count = employeeService.insertExecutive(employee);
        System.out.println(count + "행이 등록되었습니다.");
        System.out.println();

        System.out.println("#15 연예관계자인 손진현님을 연예관계자 목록에서 제거");
        int count2=  employeeService.deleteExecutive(employee);
        System.out.println(count2 + "행이 삭제되었습니다.");

//        System.out.println("여러 사용자 목록 조회");
//        List<String> noList = Arrays.asList("20110301", "20110201");
//        studentList = service.getStudentListByNo(noList);
//        studentList.forEach(System.out::println);
//        System.out.println();

//        System.out.println("사용자 등록");
//        final department student = new department("20111201", "테스트", "2000-01-01");
//        count = service.insertStudent(student);
//        System.out.println(count + "행이 등록되었습니다.");
//        System.out.println();

//        System.out.println("사용자 목록 등록");
//        final List<department> newStudentList1 =  Arrays.asList(
//                new department("20130101", "테스트1", "2000-01-01"),
//                new department("20130201", "테스트2", "2000-01-01"),
//                new department("20130301", "테스트3", "2000-01-01"),
//                new department("20130401", "테스트4", "2000-01-01")
//        );
//
//        final StopWatch stopWatch = new StopWatch();
//        stopWatch.start();
//        count = service.insertStudentList(newStudentList1);
//        stopWatch.stop();
//        System.out.println(count + "행이 등록되었습니다. [소요시간] " + stopWatch.getNanoTime());
//        System.out.println();

//        System.out.println("사용자 목록 등록");
//        final List<department> newStudentList2 =  Arrays.asList(
//                new department("20140101", "테스트5", "2000-01-01"),
//                new department("20140201", "테스트6", "2000-01-01"),
//                new department("20140301", "테스트7", "2000-01-01"),
//                new department("20140401", "테스트8", "2000-01-01")
//        );
//
//        stopWatch.reset();
//        stopWatch.start();
//        count = service.insertStudentListBatch(newStudentList2);
//        stopWatch.stop();
//        System.out.println(count + "행이 등록되었습니다. [소요시간] " + stopWatch.getNanoTime());
//        System.out.println();
    }
}
