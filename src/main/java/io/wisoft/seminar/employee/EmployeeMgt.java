package io.wisoft.seminar.employee;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class EmployeeMgt {

    private String name;
    private String management;

    public EmployeeMgt() {

    }

    public EmployeeMgt(String name, String management) {
        this.name = name;
        this.management = management;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("emp_name", name)
                .append("emp_mgt", management)
                .toString();
    }
}
