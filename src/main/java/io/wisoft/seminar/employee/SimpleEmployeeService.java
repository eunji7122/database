package io.wisoft.seminar.employee;

import io.wisoft.seminar.configure.ConnectionMaker;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class SimpleEmployeeService implements EmployeeService {

    final ConnectionMaker connectionMaker =  new ConnectionMaker();

//    @Override
//    public int insertStudent(Drama department) {
//        int result = 0;
//        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
//            final DramaService service = sqlSession.getMapper(DramaService.class);
//            result = service.insertStudent(department);
//            sqlSession.commit();
//        } catch (final Exception e){
//            e.printStackTrace();
//        }
//
//        return result;
//    }
//
//    @Override
//    public int insertStudentList(List<Drama> departmentList) {
//        int result = 0;
//        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
//            final DramaService service = sqlSession.getMapper(DramaService.class);
//            result = service.insertStudentList(departmentList);
//            sqlSession.commit();
//        } catch (final Exception e){
//            e.printStackTrace();
//        }
//
//        return result;
//    }
//
//    @Override
//    public int insertStudentListBatch(List<Drama> departmentList) {
//        int result = 0;
//        try (final SqlSession sqlSession = connectionMaker.getSqlSession(ExecutorType.BATCH)) {
//            final DramaService service = sqlSession.getMapper(DramaService.class);
//            for (final Drama department : departmentList){
//                result += service.insertStudent(department);
//            }
//            sqlSession.flushStatements();
//            sqlSession.commit();
//        } catch (final Exception e){
//            e.printStackTrace();
//        }
//
//        return result;
//    }


    @Override
    public List<Employee> getEmployeeAll() {
        List<Employee> employeeList = new ArrayList<>();

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
            final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
            employeeList = service.getEmployeeAll();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return employeeList;
    }

    @Override
    public List<EmployeeSalary> getSalaryAll() {
        List<EmployeeSalary> employeeList = new ArrayList<>();

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()){
            final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
            employeeList = service.getSalaryAll();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return employeeList;
    }

    @Override
    public List<EmployeeMgt> getNameMgt() {
        List<EmployeeMgt> employeeList = new ArrayList<>();

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()){
            final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
            employeeList = service.getNameMgt();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return employeeList;
    }

    @Override
    public List<Employee> getNameSalary() {
        List<Employee> employeeList = new ArrayList<>();

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
            final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
            employeeList = service.getNameSalary();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return employeeList;
    }

    @Override
    public List<EmployeeRank> getEmployeeRank() {
        List<EmployeeRank> employeeList = new ArrayList<>();

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
            final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
            employeeList = service.getEmployeeRank();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return employeeList;
    }

    @Override
    public List<Employee> getMoreAvg() {
        List<Employee> employeeList = new ArrayList<>();

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
            final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
            employeeList = service.getMoreAvg();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return employeeList;
    }

    @Override
    public int updateRankSalary() {
        int result = 0;

        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
            final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
            result = service.updateRankSalary();
            sqlSession.commit();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int insertExecutive(Employee employee) {
        int result = 0;
        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
            final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
            result = service.insertExecutive(employee);
            sqlSession.commit();
        } catch (final Exception e){
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public int deleteExecutive(Employee employee) {
        int result = 0;
        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
            final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
            result = service.deleteExecutive(employee);
            sqlSession.commit();
        } catch (final Exception e){
            e.printStackTrace();
        }

        return result;
    }
    //    @Override
//    public List<Drama> getDepartmentListByNo(List<String> studentNoList) {
//        List<Drama> employeeList = new ArrayList<>();
//
//        try (final SqlSession sqlSession = connectionMaker.getSqlSession()) {
//            final DramaService service = sqlSession.getMapper(DramaService.class);
//            employeeList = service.getDepartmentListByNo(studentNoList);
//        } catch (final Exception e) {
//            e.printStackTrace();
//        }
//
//        return employeeList;
//    }
}
