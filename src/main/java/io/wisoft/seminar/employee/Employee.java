package io.wisoft.seminar.employee;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Employee {

    private String code;
    private String name;
    private String management;
    private int salary;

    public Employee() {
    }

    public Employee(String code, String name, String management, int salary){
        this.code = code;
        this.name = name;
        this.management = management;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("emp_code", code)
                .append("emp_name", name)
                .append("emp_mgt", management)
                .append("emp_sal", salary)
                .toString();
    }
}
