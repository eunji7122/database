package io.wisoft.seminar.employee;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class EmployeeRank {

    private String rank;
    private float avg;
    private float min;
    private float max;

    public EmployeeRank() {

    }

    public EmployeeRank(String rank, float avg, float min, float max) {
        this.rank = rank;
        this.avg = avg;
        this.min = min;
        this.max = max;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("emp_rname", rank)
                .append("avg", avg)
                .append("min", min)
                .append("max", max)
                .toString();
    }
}
