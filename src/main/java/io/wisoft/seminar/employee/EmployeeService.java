package io.wisoft.seminar.employee;

import java.util.List;

public interface EmployeeService {

//    int insertDepartment(final Drama Drama);
//
//    int insertDepartmentList(final List<Drama> employeeList);
//
//    int insertDepartmentListBatch(final List<Drama> employeeList);

    List<Employee> getEmployeeAll();

    List<EmployeeSalary> getSalaryAll();

    List<EmployeeMgt> getNameMgt();

    List<Employee> getNameSalary();

    List<EmployeeRank> getEmployeeRank();

    List<Employee> getMoreAvg();

    int updateRankSalary();

    int insertExecutive(final Employee employee);

    int deleteExecutive(final Employee employee);

//    List<Drama> getDepartmentListByNo(final List<String> studentNo);

}
