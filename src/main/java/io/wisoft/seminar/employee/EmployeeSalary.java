package io.wisoft.seminar.employee;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class EmployeeSalary {

    private float sum;
    private float avg;

    public EmployeeSalary(){

    }

    public EmployeeSalary(float sum, float avg) {
        this.sum = sum;
        this.avg = avg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("sum", sum)
                .append("avg", avg)
                .toString();
    }
}
